$(function(){
	buildDisplay();
	for(var gen=0; gen < 50; gen++){
		createCircle();
	}

	$("#genCircleBtn").click(function(e){createCircle()});
	$("#colorsBtn").click(function(e){changeColors()});
	$("#resetBtn").click(function(e){reset()});
	$("#animateBtn").click(function(e){startStop()});

})

//HINTS
//Give each circle its own event handlers so that it moves itself
	//Create each circle as its own html object
	//treat it as a javascript object
	//Each circle handles its own animation

	//google "animate set interval javascript"

//z-index brings circle to top
//track z-index globally and apply to clicked on circle

var circIndex = 0;
var zIndexFront = 1;
var move = 0;
var inter;
var cTopMove = [];
var cLeftMove = [];

function buildDisplay(){
	$("#menu").append("<button type=\"button\" class=\"btn\" id=\"genCircleBtn\">Add Circle</button>");
	$("#menu").append("<button type=\"button\" class=\"btn\" id=\"colorsBtn\">Change Colors</button>");
	$("#menu").append("<button type=\"button\" class=\"btn\" id=\"resetBtn\">Reset</button>");
	$("#menu").append("<button type=\"button\" class=\"btn\" id=\"animateBtn\">Start/Stop Animation</button>");
	$("#page").append("<div class=\"display\" id=\"screen\"></div>");
}

function createCircle(){
	$("#screen").append("<div class=\"circle\" id=\"circle" + circIndex.toString() + "\"></div>");

	$("#circle" + circIndex.toString()).css({
		'background-color' : getRandomColor(),
		'border' : '2px solid ' + getRandomColor(),
		'top' : randomTopPosition() + 'px',
		'left' : randomLeftPosition() + 'px'
	});

	$("#circle" + circIndex.toString()).click(function(e){$(this).css('z-index', bringToFront())});

	$("#circle" + circIndex.toString()).dblclick(function(e){$(this).remove()});

	giveDirection();

	circIndex++;
}

function changeColors(){
	for(var i=0; i < circIndex; i++){
		$("#circle" + i.toString()).css({
			'background-color' : getRandomColor(),
			'border' : '2px solid ' + getRandomColor()
		});
	}
}

function reset(){
	for(var i=0; i < circIndex; i++){
		$("#circle" + i.toString()).remove();
	}

	circIndex = 0;

	for(var i=0; i<50; i++){
		createCircle();
	}
}

function startStop(){
	if(move == 0){
		move++;
		moveElements();
	}
	else{
		move--;
		clearInterval(inter);
	}
}

function moveElements(){

	for(var i=0; i<circIndex; i++){
		var r1 = Math.random();
		var r2 = Math.random();

		if(r1 <= 0.33)
			cTopMove[i] = 1;
		else if (r1 <= 0.66)
			cTopMove[i] = -1;
		else
			cTopMove[i] = 0;	

		if(r2 <= 0.33)
			cLeftMove[i] = 1;
		else if (r2 <= 0.66)
			cLeftMove[i] = -1;
		else
			cLeftMove[i] = 0;

		if(cTopMove[i] == 0 && cLeftMove[i] == 0)
			i--;
	}


	inter = setInterval(function(){
		for(var i=0; i<circIndex; i++){
			var circ = $("#circle" + i.toString())
			var cTop = parseInt(circ.css("top"));
			var cLeft = parseInt(circ.css("left"));

			if(cTop <= 112 || cTop >= 608){
				cTopMove[i] = cTopMove[i] * -1;
				circ.css({
					'background-color' : getRandomColor(),
					'border' : '2px solid ' + getRandomColor()
				});
			}

			if(cLeft <= 252 || cLeft >= 748){
				cLeftMove[i] = cLeftMove[i] * -1;
				circ.css({
					'background-color' : getRandomColor(),
					'border' : '2px solid ' + getRandomColor()
				});
			}

			cTop += cTopMove[i];
			cLeft += cLeftMove[i];

			circ.css({
				'top' : cTop.toString() + 'px',
				'left' : cLeft.toString() +'px'
			});		
		}
	}, 100);
}

function giveDirection(){

	var r1 = Math.random();
	var r2 = Math.random();

	if(r1 <= 0.33)
		cTopMove[circIndex] = 1;
	else if (r1 <= 0.66)
		cTopMove[circIndex] = -1;
	else
		cTopMove[circIndex] = 0;

	if(r2 <= 0.33)
		cLeftMove[circIndex] = 1;
	else if (r2 <= 0.66)
		cLeftMove[circIndex] = -1;
	else
		cLeftMove[circIndex] = 0;

	if(cTopMove[circIndex] == 0 && cLeftMove[circIndex] == 0)
		giveDirection();
}

function bringToFront(){
	zIndexFront++;
	return zIndexFront;
}

function randomLeftPosition(){
	return (Math.random() * 496 + 252).toString();
}

function randomTopPosition(){
	return (Math.random() * 496 + 112).toString();
}

function getRandomColor(){
	var letters= "0123456789abcdef";
	var result = "#";

	for(var i=0; i<6; i++){
		result += letters.charAt(parseInt(Math.random() * letters.length));
	}

	return result;
}